CPPFLAGS=-std=c++11 -pedantic -Wall -Werror -ggdb3
efficient_frontier: efficient_frontier.o
		g++ -o efficient_frontier $(CCFLAGS) efficient_frontier.o
efficient_frontier.o: efficient_frontier.cpp
		g++ -c $(CCFLAGS) efficient_frontier.cpp
clean:
		rm -f *.o *~ efficient_frontier


