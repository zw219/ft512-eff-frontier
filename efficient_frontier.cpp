#include <string>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <vector>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Eigen;

int main(int argc, char ** argv) {
	if (argc != 4 && argc != 3) {
		cerr << "./efficient_frontier (-r) universe.csv correlation.csv" << endl;
    	exit(EXIT_FAILURE);
	}
	bool r = false;
	if (argc == 4) {
		string flag(argv[1]);
        if (flag == "-r") {
            r = true;
        }
		else {
			cerr << "./efficient_frontier (-r) universe.csv correlation.csv" << endl;
    		exit(EXIT_FAILURE);
		}
	}

	// read files
	ifstream u_stream(argv[argc - 2], ifstream::in);
	ifstream c_stream(argv[argc - 1], ifstream::in);
	if (u_stream.fail() || c_stream.fail()) {
	    cerr << "failed to open one of the files." << endl;
	    exit(EXIT_FAILURE);
  	}

  	vector<double> aveReturn;
  	vector<double> sd;

  	string line;
  	while (u_stream.good() && getline(u_stream, line)) {
  		stringstream ssline(line);
  		vector<string> tokens;
  		for (string token; getline(ssline, token, ','); tokens.push_back(token));
  		// to catch errors
  		try {
  			aveReturn.push_back(atof(tokens[1].c_str()));
  			sd.push_back(atof(tokens[2].c_str()));
  		} catch(...) {
	    	cerr << "failed to read universe" << endl;
	    	exit(EXIT_FAILURE);
  		}
  	}
  	
  	int size = sd.size();
  	vector<vector<double> > cov(size);
  	for (int i = 0; i < size; i++) {
		if (!c_stream.good() || !getline(c_stream, line)) {
			cerr << "Wrong file format." << endl;
			exit(EXIT_FAILURE);
		}
		stringstream ssline(line);
		string token;
		for (int j = 0; j < size; j++) {
			if (!getline(ssline, token, ',')) {
				cerr << "Wrong line format" << endl;
				exit(EXIT_FAILURE);
      		}
      		cov[i].push_back(atof(token.c_str()) * sd[i] * sd[j]);
		}
		if (getline(ssline, token, ',')) {
			cerr << "Wrong line format" << endl;
			exit(EXIT_FAILURE);
		}
	}
	if (c_stream.good() && getline(c_stream, line)) {
		cerr << "Wrong file format" << endl;
		exit(EXIT_FAILURE);
	}


	// calcualte and print
	int n = size + 2;
	cout << "ROR,volatility" << endl;
	for (int i = 1; i <= 26; i++) {
		MatrixXd A(n, n);
		MatrixXd B(n, 1);
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				A(x, y) = cov[x][y];
			}
			B(x, 0) = 0;
			A(x, size) = 1;
			A(size, x) = 1;
			A(x, size + 1) = aveReturn[x];
			A(size + 1, x) = aveReturn[x];
		}
		for (int x = size; x < n; x++) {
			for (int y = size; y < n; y++) {
				A(x, y) = 0;
			}
		}
		B(size, 0) = 1;
		B(size + 1, 0) = 0.01 * i;
		ColPivHouseholderQR<MatrixXd> dec(A);
	    MatrixXd C = dec.solve(B);
	    if (r) {
	    	// store all negative weight positions
	    	vector<int> negatives;
	    	for (int i = 0; i < size; i++) {
	    		if (C(i, 0) < 0) {
	    			negatives.push_back(i);
	    		}
	    	}
	    	// until no negative values
	    	while(!negatives.empty()) {
	    		int current = A.rows();
	    		int numNega = negatives.size();
	    		// reculculate
	    		MatrixXd tempA(current + numNega, current + numNega);
	    		MatrixXd tempB(current + numNega, 1);
	    		for (int x = 0; x < current + numNega; x++) {
	    			for (int y = 0; y < current + numNega; y++) {
	    				tempA(x, y) = 0;
	    			}
	    			tempB(x, 0) = 0;
	    		}
	    		// copy AB
	    		for (int x = 0; x < current; x++) {
	    			for (int y = 0; y < current; y++) {
	    				tempA(x, y) = A(x, y);
	    			}
	    			tempB(x, 0) = B(x, 0);
	    		}
	    		// set equition
	    		for (int i = 0; i < numNega; i++) {
	    			tempA(negatives[i], current + i) = 1.0;
	    			tempA(current + i, negatives[i]) = 1.0;
	    		}
	    		negatives.clear();
	    		A = tempA;
	    		B = tempB;
	    		ColPivHouseholderQR<MatrixXd> dec(A);
    			C = dec.solve(B);
    			for (int i = 0; i < size; i++) {
      				if (C(i, 0) < 0) {
       					negatives.push_back(i);
      				}
    			} 
	    	}
	    }
	    double result = 0.0;
	    for (int x = 0; x < size; x++) {
	    	for (int y = 0; y < size; y++) {
	    		result += C(x, 0) * C(y, 0) * cov[x][y];
	    	}
	    }
	    cout << fixed << setprecision(1) << (double) i << "%," << setprecision(2) << sqrt(result) * 100 << "%" << endl;
	}
  	u_stream.close();
  	c_stream.close();
  	return EXIT_SUCCESS;
}

